﻿using CRUD.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRUD.Context {
    public class ApplicationDBContext : DbContext, IApplicationDBContext {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }

        
    }
}