﻿using CRUD.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CRUD.Context {
    public interface IApplicationDBContext :IDisposable, IObjectContextAdapter  {

        DbSet<Post> Posts { get; set; }
        DbSet<User> Users { get; set; }
    }
}