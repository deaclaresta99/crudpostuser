﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRUD.Models {
    public class User {

        [Key]
        public int UserID { get; set; }

        [Required(ErrorMessage = "Full Name is required.")]
        public string Full_Name { get; set; }

        [Required(ErrorMessage = "Email  is required.")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Username is required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Birth Date is required.")]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Phone Number is required.")]
        public string phone_number { get; set; }


        public virtual List<Post> Posts { get; set; }
    }
}