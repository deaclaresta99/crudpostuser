﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRUD.Models {
    public class Post {

        [Key]
        public int PostId { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        [MaxLength(30)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Slug is required.")]
        [MaxLength(20)]
        public string Slug { get; set; }

        [Required(ErrorMessage = "Content is required.")]
        [MaxLength(200)]
        public string Content { get; set; }


        public int UserId { get; set; }
        public virtual User User { get; set; }

    }
}