﻿using CRUD.Models;
using CRUD.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRUD.Services.Business {
    public class SecurityService : ISecurityService {
        SecurityDAO daoServices = new SecurityDAO();
        public bool Authenticate(User user) {

            return daoServices.FindByUser(user);
        }
    }
}