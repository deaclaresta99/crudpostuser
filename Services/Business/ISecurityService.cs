﻿using CRUD.Models;

namespace CRUD.Services.Business {
    public interface ISecurityService {
        bool Authenticate(User user);
    }
}