﻿using CRUD.Models;
using CRUD.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CRUD.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user ) {
            //using (var context = new OfficeEntities()) {

            //}
            SecurityService securityServices = new SecurityService();
            Boolean result = securityServices.Authenticate(user);
            if (result) {
                FormsAuthentication.SetAuthCookie(user.Username,false);
                return RedirectToAction("Index","Users");
                
            } else {
                ModelState.AddModelError("", "Failed to log in ");
                return View();
            }
            //return View();
        }

        public ActionResult Logout() {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        //[HttpPost]
        //public ActionResult Login(User user) {
        //    return View();
        //}
    }
}